#ifndef __FORMATS_H__
#define __FORMATS_H__

#include "evpath.h"

typedef struct _l1 {
  int64_t natoms;
  int64_t nbonds;
  int64_t nangles;
  int64_t ndihedrals;
} l1_t, *l1_ptr_t;

static FMField l1_field_list[] = {
  {"natoms", "integer", sizeof(int64_t), FMOffset(l1_ptr_t, natoms)},
  {"nbonds", "integer", sizeof(int64_t), FMOffset(l1_ptr_t, nbonds)},
  {"nangles", "integer", sizeof(int64_t), FMOffset(l1_ptr_t, nangles)},
  {"ndihedrals", "integer", sizeof(int64_t), FMOffset(l1_ptr_t, ndihedrals)},
  { NULL, NULL }
};

static FMStructDescRec l1_format_list[] = {
  {"l1", l1_field_list, sizeof(l1_t), NULL },
  { NULL, NULL }
};

typedef struct _l2 {
  int64_t quantity;
} l2_t, *l2_ptr_t;

static FMField l2_field_list[] = {
  {"quantity", "integer", sizeof(int64_t), FMOffset(l2_ptr_t, quantity)},
  { NULL, NULL }
};

static FMStructDescRec l2_format_list[] = {
  {"l2", l2_field_list, sizeof(l2_t), NULL },
  { NULL, NULL }
};

#endif /* __FORMATS_H__ */

