#include <fstream>
#include <iostream>
#include <exception>

using namespace std;

#include "json/json.h"

extern "C" {
#include "evpath.h"
#include "ev_dfg.h"
}

#include "formats.h"


EVdfg
create_dfg_from_json ( EVmaster& master, const string& fname,
		       std::map<string, EVdfg_stone>& stonemap)
{
  EVdfg dfg = EVdfg_create( master );
  Json::Value root;

  try {
    std::ifstream doc( fname /* JSON file name */, std::ifstream::binary );
    doc >> root;
  }
  catch ( std::exception& e ) {
    std::cerr << "std::exception while opening/parsing JSON file (" << fname << ")" 
	      << e.what()
	      << endl;
  }

  /* 
   * get list of node names and build stone map 
   */
  string master_name;
      
  Json::Value nodes = root["nodes"];
  Json::Value edges = root["links"];
  
  /*
   *  Gather all the node names for EVdfg
   */
  auto nodelist = new char*[nodes.size() + 1];
  size_t j = 0;
  for (auto vi = nodes.begin(); vi != nodes.end(); vi++, j++) {
    nodelist[j] = strdup( (*vi)["nodename"].asCString() );
    //if (vi->isMember( "master" )) master_name = name;
  }
  nodelist[j] = 0;  /* satisfy EVpath null-terminated array fetish */
  EVmaster_register_node_list( master, nodelist );

  /*
   *  Now we can register everything in the nodes list
   */
  for (auto vi = nodes.begin(); vi != nodes.end(); vi++) {
    EVdfg_stone s;
    string n = (*vi)["nodename"].asString();
    
    if (vi->get("source", false).asBool()) {
      
      /* this node thinks its a source */
      cerr << "register source " << n << endl;
      char *c = const_cast<char*>((*vi)["register-as"].asCString());
      s = EVdfg_create_source_stone( dfg, c );
      
    }
    else if (vi->get( "sink", false ).asBool()) {
      
      /* this node thinks its a sink */
      cerr << "register sink " << n << endl;
      char *c = const_cast<char*>( (*vi)["register-as"].asCString() );
      s = EVdfg_create_sink_stone( dfg, c );
      
    }
    else {
      char *action_spec = NULL;
      /* regular old stone */
      /* check for operation */
      if (vi->isMember( "operation" )) {
	action_spec =
	  create_transform_action_spec
	  (
	   l1_format_list, l2_format_list,
	   const_cast<char*>( (*vi)["operation"].asCString() )
	   );
      }
      cerr << "register stone " << n << endl;
      s = EVdfg_create_stone( dfg, action_spec );
    }

    EVdfg_assign_node( s, const_cast<char*>(n.c_str()) );
    stonemap[n] = s;
  }

  /* Link the stones according to the edge list */
  for (auto vi = edges.begin(); vi != edges.end(); vi++) {
    /* 
       get the source and target indices from the edge list.
       use them to index into the nodes list To Get Names
       use names to index the stonemap to get stone ids
       link the stones
    */
    int s, t;
    s = (*vi)["source"].asInt();
    t = (*vi)["target"].asInt();
    /* the indexes are 1-based */
    const string& ss = nodes[s-1]["nodename"].asString();
    const string& ts = nodes[t-1]["nodename"].asString();
    cerr << "link " << ss << " " << stonemap[ss] << " to " << ts << " " << stonemap[ts] << endl;
    EVdfg_link_dest( stonemap[ss], stonemap[ts] );
    
  }

  EVdfg_realize( dfg );
  return dfg;
}
