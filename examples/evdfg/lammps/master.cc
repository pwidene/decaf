extern "C" {
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "evpath.h"
#include "ev_dfg.h"
}
#include "mpi.h"
#include "lammps.h"
#include "input.h"
#include "atom.h"
#include "library.h"

#include "formats.h"

using namespace LAMMPS_NS;

EVclient test_client;

static int
simple_handler(CManager cm, void *vevent, void *client_data, attr_list attrs)
{
  auto event = reinterpret_cast<l1_ptr_t> (vevent);
  printf("I got %lli\n", event->natoms);
  EVclient_shutdown(test_client, event->natoms == -1);
  return 1;
}


int main(int argc, char **argv)
{
  LAMMPS *lmp = NULL;
  MPI_Init( &argc, &argv );
  int myrank, num_ranks, toprank;

  char *nodes[] = {"a", "b", "c", NULL};
  CManager cm;
  EVmaster test_master;
  EVdfg test_dfg;
  char *str_contact;
  EVdfg_stone src, mid, sink;
  EVsource source_handle;
  EVclient_sources source_capabilities = NULL;
    
  MPI_Comm_rank( MPI_COMM_WORLD, &myrank );
  MPI_Comm_size( MPI_COMM_WORLD, &num_ranks );
  toprank = num_ranks - 1;
  
  int lammps_procs = atoi(argv[1]);
  if (lammps_procs > num_ranks) {
    if (myrank == 0)
      fprintf( stderr, "LAMMPS proc count exceeds MPI job proc count; goodbye.\n");
    MPI_Abort( MPI_COMM_WORLD, 1 );
  }

  MPI_Comm comm_lammps;
  int color = (myrank < lammps_procs) ? 1 : MPI_UNDEFINED;
  MPI_Comm_split( MPI_COMM_WORLD, color, 0, &comm_lammps );
  
  if (myrank == toprank) {

    cm = CManager_create();
    CMlisten(cm);

    test_master = EVmaster_create(cm);
    str_contact = EVmaster_get_contact_list(test_master);
    EVmaster_register_node_list(test_master, &nodes[0]);

    /*
    **  LOCAL DFG SUPPORT   Sources and sinks that might or might not be utilized.
    */

    source_handle = EVcreate_submit_handle(cm, -1, l1_format_list);
    source_capabilities = EVclient_register_source("event source", source_handle);

    /*
    **  DFG CREATION
    */
    test_dfg = EVdfg_create(test_master);

    src = EVdfg_create_source_stone(test_dfg, "event source");
    EVdfg_assign_node(src, "a");

    mid = EVdfg_create_stone(test_dfg, NULL);
    EVdfg_assign_node(mid, "b");
    EVdfg_link_port(src, 0, mid);
    
    sink = EVdfg_create_sink_stone(test_dfg, "l1_handler");
    EVdfg_assign_node(sink, "c");
    EVdfg_link_port(mid, 0, sink);

    EVdfg_realize(test_dfg);

    /* We're node "a" in the DFG */
    test_client = EVclient_assoc_local(cm, "a", test_master, source_capabilities, NULL);

    printf("Contact list is \"%s\"\n", str_contact);
    if (EVclient_ready_wait(test_client) != 1) {
      /* dfg initialization failed! */
      exit(1);
    }

  }

  FILE *fp = NULL;
  if (myrank == toprank) {
    fp = fopen( argv[2], "r" );
    if (!fp) {
      fprintf( stderr, "Can't open input script; goodbye.\n" );
      MPI_Abort( MPI_COMM_WORLD, 1 );
    }
  }

  // all the worker processes pick up a hammer
  if (color == 1) lmp = new LAMMPS( 0, NULL, comm_lammps );

    int n;
    char line[1024];
    while (1) {
      if (myrank == toprank) {
	if (fgets(line,1024,fp) == NULL) n = 0;
	else n = strlen(line) + 1;
	if (n == 0) fclose(fp);
      }

      MPI_Bcast( &n, 1, MPI_INT, toprank, MPI_COMM_WORLD );
      if (n == 0) break;
      MPI_Bcast( line, n, MPI_CHAR, toprank, MPI_COMM_WORLD);
      if (color == 1) lmp->input->one(line);
    }
    
  if (color == 1) {
    l1_t msg;
      
    lmp->input->one( "run 10" );

    if (myrank == toprank) {
      msg.natoms = static_cast<int64_t> ( lmp->atom->natoms );
      msg.nbonds = static_cast<int64_t> ( lmp->atom->nbonds );
      msg.nangles = static_cast<int64_t> ( lmp->atom->nangles );
      msg.ndihedrals = static_cast<int64_t> ( lmp->atom->ndihedrals );

      if (EVclient_source_active( source_handle)) {
	EVsubmit( source_handle, &msg, NULL );
      }
    }
	
    double *x = new double[3*lmp->atom->natoms];	
    lammps_gather_atoms( lmp, "x", 1, 3, x );
    double epsilon = 0.1;
    x[0] += epsilon;
    lammps_scatter_atoms( lmp, "x", 1, 3, x );
    delete [] x;

    lmp->input->one( "run 1" );

    if (myrank == toprank) {
      msg.natoms = static_cast<int64_t> ( lmp->atom->natoms );
      msg.nbonds = static_cast<int64_t> ( lmp->atom->nbonds );
      msg.nangles = static_cast<int64_t> ( lmp->atom->nangles );
      msg.ndihedrals = static_cast<int64_t> ( lmp->atom->ndihedrals );

      if (EVclient_source_active( source_handle)) {
	EVsubmit( source_handle, &msg, NULL );
      }
    }

    //
    // Indicate shutdown to the DFG
    //
    if (myrank == toprank) {
      msg.natoms = -1;
      if (EVclient_source_active( source_handle))
	EVsubmit( source_handle, &msg, NULL );
    }
  }

  if (color == 1) delete lmp;

  if (myrank == toprank) {
    EVclient_shutdown( test_client, DFG_STATUS_SUCCESS );
    EVclient_wait_for_shutdown( test_client );
  }

  MPI_Finalize();

  return 0;
}
