#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "evpath.h"
#include "ev_dfg.h"

#include "formats.h"

EVclient test_client;

static int
l1_handler(CManager cm, void *vevent, void *client_data, attr_list attrs)
{
  l1_ptr_t event = reinterpret_cast<l1_ptr_t> (vevent);

  if (event->natoms == -1)
    EVclient_shutdown( test_client, DFG_STATUS_SUCCESS );
  else
    printf("Received event with %lli atoms\n", event->natoms);    
  return 1;
}

static int
l2_handler(CManager cm, void *vevent, void *client_data, attr_list attrs)
{
  l2_ptr_t event = reinterpret_cast<l2_ptr_t>( vevent );

  if (event->quantity == -1)
    EVclient_shutdown( test_client, DFG_STATUS_SUCCESS );
  else
    printf("Received nonsense quantity %lli\n", event->quantity);
  return 1;
}

/* this file is evpath/examples/dfg_client3.c */
int main(int argc, char **argv)
{
  CManager cm;
  EVsource source_handle;
  EVclient_sinks sink_capabilities;
  EVclient_sources source_capabilities;
  
  (void)argc; (void)argv;
  cm = CManager_create();
  CMlisten(cm);

  /*
  **  LOCAL DFG SUPPORT   Sources and sinks that might or might not be utilized.
  */

  source_handle = EVcreate_submit_handle(cm, -1, l1_format_list);
  source_capabilities = EVclient_register_source("event source", source_handle);
  EVclient_register_sink_handler(cm, "l1_handler", l1_format_list,
				 (EVSimpleHandlerFunc) l1_handler, NULL);
  sink_capabilities = EVclient_register_sink_handler(cm, "l2_handler", l2_format_list,
						     (EVSimpleHandlerFunc) l2_handler, NULL);
  
  /* We're node argv[1] in the process set, contact list is argv[2] */
  test_client = EVclient_assoc(cm, argv[1], argv[2], source_capabilities, sink_capabilities);
  
  if (EVclient_ready_wait(test_client) != 1) {
    /* dfg initialization failed! */
    exit(1);
  }

    
  if (EVclient_source_active(source_handle)) {
    l1_t rec;
    rec.natoms = -1;
    /* submit would be quietly ignored if source is not active */
    EVsubmit(source_handle, &rec, NULL);
    }

  if (EVclient_active_sink_count(test_client) > 0) {
    /* if there are active sinks, the handler will call EVclient_shutdown() */
  } else {
    if (EVclient_source_active(source_handle)) {
      /* we had a source and have already submitted, indicate success */
      EVclient_shutdown(test_client, 0 /* success */);
    } else {
      /* we had neither a source or sink, ready to shutdown, no opinion */
      EVclient_ready_for_shutdown(test_client);
    }
  }
  
  return(EVclient_wait_for_shutdown(test_client));
}
