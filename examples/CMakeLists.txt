add_subdirectory            (direct)

add_subdirectory            (redist)
add_subdirectory            (C)
#add_subdirectory            (flowvr)
if                          (LAMMPS_INCLUDE_DIR AND
                             LAMMPS_LIBRARY     AND
                             FFTW_INCLUDE_DIR   AND
                             FFTW_LIBRARY)
  add_subdirectory          (lammps)
  message                   ("Building lammps example")
else                        ()
  message                   ("Don't have LAMMPS_INCLUDE_DIR, LAMMPS_LIBRARY, FFTW_INCLUDE_DIR, and FFTW_LIBRARY; not building lammps example")
endif                       ()

if                          (hacc)
  add_subdirectory          (hacc)
endif                       ()

#add_subdirectory          (block_data_model)

# add_subdirectory            (cfd)
